#pragma once

#include <sys/types.h>

/*
 * Find value in [begin, end)
 * returns end if value is not found
 */
const char* find_plain(const char* begin, const char* end, char value);
const char* find_simd(const char* begin, const char* end, char value);
