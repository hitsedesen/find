#include <benchmark/benchmark.h>
#include <random>

std::string randomString(size_t x)
{
    static std::random_device randomDevice;
    static std::mt19937_64 randomEngine(randomDevice());
    constexpr std::string_view Alphabet{"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"};
    static std::uniform_int_distribution<size_t> alphabetDistribution(0, Alphabet.size());
    std::string result;
    std::generate_n(std::back_inserter(result), x, [&] {
        return Alphabet[alphabetDistribution(randomEngine)];
    });
    return result;
};

static void BM_FindPlain(benchmark::State& state)
{
    auto size = state.range(0);
    auto str = randomString(size);

    for (auto _ : state) {
        benchmark::DoNotOptimize(find(str.begin(), str.end(), '!'));
    }
    state.SetBytesProcessed(state.max_iterations * size);
}

BENCHMARK(BM_FindPlain)->Range(8, 8 << 20);


static void BM_FindSIMD(benchmark::State& state)
{
    auto size = state.range(0);
    auto str = randomString(size);

    for (auto _ : state) {
        benchmark::DoNotOptimize(find(str.begin(), str.end(), '!'));
    }
    state.SetBytesProcessed(state.max_iterations * size);
}

BENCHMARK(BM_FindSIMD)->Range(8, 8 << 20);


BENCHMARK_MAIN();
