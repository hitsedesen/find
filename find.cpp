#include "find.hpp"

#include <bit>
#include <cstdint>
#include <emmintrin.h>
#include <immintrin.h>
#include <mmintrin.h>
#include <xmmintrin.h>

const char* find_plain(const char* begin, const char* end, char value)
{
    for (auto it = begin; it != end; ++it) {
        if (*it == value) {
            return it;
        }
    }

    return end;
}

const char* find_simd(const char* begin, const char* end, char value)
{
    auto it = begin;
#if 1
    __m256i v256 = _mm256_set1_epi8(value);
    for (; (end - it) > 32; it += 32) {
        __m256i r = _mm256_cmpeq_epi8(v256, *reinterpret_cast<const __m256i*>(it));
        unsigned int mask = _mm256_movemask_epi8(r);
        int index = std::countr_zero(mask);
        if (index < 32) {
            return it + index;
        }
    }
#endif
#if 0
    __m128i v128 = _mm_set1_epi8(value);
    for (; (end - it) > 16; it += 16) {
        __m128i r = _mm_cmpeq_epi8(v128, *reinterpret_cast<const __m128i*>(it));
        unsigned int mask = _mm_movemask_epi8(r);
        int index = std::countr_zero(mask);
        if (index < 16) {
            return it + index;
        }
    }
#endif
#if 0
    __m64 v64 = _mm_set1_pi8(value);
    for (; (end - it) > 8; it += 8) {
        __m64 r = _mm_cmpeq_pi8(v64, *reinterpret_cast<const __m64*>(it));
        unsigned int mask = _mm_movemask_pi8(r);
        int index = std::countr_zero(mask);
        if (index < 8) {
            return it + index;
        }
    }
#endif
    return find_plain(it, end, value);
}
